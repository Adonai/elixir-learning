defmodule KV.RegistryTest do
  use ExUnit.Case, async: true

  setup do
    # `start_supervised` guarantees that the registry process will be shutdown before the next test starts
    registry = start_supervised!(KV.Registry)
    %{registry: registry}
  end

  test "spawns buckets", %{registry: registry} do
    assert KV.Registry.lookup(registry, "foo") == :error

    KV.Registry.create(registry, "foo")
    assert {:ok, bucket} = KV.Registry.lookup(registry, "foo")

    KV.Bucket.put(bucket, "bar", 1)
    assert KV.Bucket.get(bucket, "bar") == 1
  end

  test "removes buckets on exit", %{registry: registry} do
    KV.Registry.create(registry, "foo")
    {:ok, bucket} = KV.Registry.lookup(registry, "foo")
    Agent.stop(bucket)

    assert KV.Registry.lookup(registry, "foo") == :error
  end
end
