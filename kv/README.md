# Key Learnings

## Agents

Agents are used for holding state that can be accessed by multiple processes, e.g. configuration.

## GenServer

These are used also for state management but are more robust than Agents. You can control what happens if the Agent it's stopped, which is not possible with just Agents. GenServers can also divide the client from server logic for a cleaner API.

Another benefit over Agents is the ability to have commands that execute asynchronously.

## Supervisor

In Elixir it's not common to use defensive programming, it is encouraged to "fail fast" and recover. If a process crashes we want to restart it. That's where Supervisors come into play, they are processes that monitor other processes (also called child processes) and are in charge of restarting them when they crash.

A Supervisor's responsibilities are:

- Start child processes
- Restart them
- Shutdown child processes

When the Supervisor starts, it will call the `child_spec/1` function on each of its child modules.
The `child_spec/1` function returns the child specification, which describes how to start the process. This is automatically defined is you are using `use Agent`, `use GenServer` or `use Supervisor` in your module.

### Supervisor Strategy

Dictates what happens when a child process crashes.
For `:one_for_one`, if a child dies, it will only restart that same child.

---

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `kv` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:kv, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/kv](https://hexdocs.pm/kv).
