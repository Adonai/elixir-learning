defmodule M do
  def greet do
    name = IO.gets("What is you name?") |> String.trim()
    IO.puts("Welcome to Elixir #{name}")
  end

  def data_stuff do
    my_int = 123
    IO.puts("Integer: #{is_integer(my_int)}")
  end

  def atoms do
    IO.puts("Is Atom: #{is_atom(:Yolo)}")
    another_atom = ":Atom with spaces"
    IO.puts("Another atom: #{:another_atom}")
  end

  def doing_strings do
    my_str = "Some string"
    IO.puts("my_str.length = #{String.length(my_str)}")

    concat_str = my_str <> " plus extra stuff"
    IO.puts(concat_str)

    # Uses a triple equals that works like JS's
    IO.puts("Checkin equality #{"Egg" === "Egg"}")

    IO.puts("my_str contains 'm'? #{String.contains?(my_str, "m")} ")

    IO.puts(
      "First char of my_str is #{String.first(my_str)} and second is #{String.at(my_str, 1)}"
    )

    IO.puts("Substring: #{String.slice(my_str, 0, 4)}")

    # Inspect is like PHP's var_dump
    IO.inspect(String.split(my_str, " "))

    # Piping
    (4 * 10) |> IO.puts()

    IO.puts("5 / 4 = #{5 / 4}")
    # div removes decimal places, returns integers
    IO.puts("div(5, 4) = #{div(5, 4)}")

    IO.puts("rem is modulo, 5 mod 3 = #{rem(5, 3)}")

    IO.puts(not true)

    unless 0 > 1 do
      IO.puts("math works")
    else
      IO.puts("WTF")
    end

    cond do
      1 < 0 ->
        IO.puts("math's broken")

      2 >= 2 ->
        IO.puts("math works")

      true ->
        IO.puts("this is like if else")

      false ->
        IO.puts(
          "it'll shortcircuit the execution, meaning that only one of the conditions will ever be executed"
        )
    end

    case 2 do
      1 -> IO.puts("it's broken")
      2 -> IO.puts("comparison on point")
      _ -> IO.puts("default case")
    end

    # Notice the commas
    ternary_example = if 1 === 2, do: "bad", else: "good"
    IO.puts(ternary_example)
  end
end
