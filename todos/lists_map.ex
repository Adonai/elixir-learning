defmodule ListsMap do
  def new(), do: %{}

  def add(lists_map, key, value) do
    Map.update(lists_map, key, [value], &[value | &1])
  end

  def get(lists_map, key) do
    Map.get(lists_map, key, [])
  end
end
