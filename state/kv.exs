# https://elixir-lang.org/getting-started/processes.html#state

# Works as a key-value store
# Note: This module is built for learning purposes, in a real app an Agent is better option
# Usage:
# > iex kv.exs
# > {:ok, pid} = KV.start_link
# > send pid, {:put, :foo, "bar"}
# > send pid, {:get, :foo, self()}
# > flush()
# "bar"
# :ok
#
# The process can also be registered so anyone with the name can use it
# > Process.register(pid, :kv)
# > send :kv, {:get, :foo, self()}
defmodule KV do
  def start_link do
    Task.start_link(fn -> loop(%{}) end)
  end

  defp loop(map) do
    receive do
      {:get, key, caller} ->
        send(caller, Map.get(map, key))
        loop(map)

      {:put, key, value} ->
        loop(Map.put(map, key, value))
    end
  end
end
